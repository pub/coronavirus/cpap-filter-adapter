# CPAP Filter Adapter for resuable mask

## Project Goal
  Create disinfectable mask with inline HEPA filter of CPAP machine (relative sufficient supply) for emergency case with no N95.

## Idea
1. SLA 3D Print mask and CPAP filter adapter with [bio-compatible resin](https://www.3dresyns.com/products/3dresyn-bioflex-a70-mf-ulwa-monomer-free-ultra-low-water-absorption?_pos=15&_sid=ba87ca4d8&_ss=r)
2. Assemble in clean room and use for the day
3. Disassemble and drop mask and adapter in to 99% isopropyl alcohol to disinfect, CPAP filter to the trash.
4. Dry both mask and adapter in clean room, ready to reuse with a new CPAP filter

## Pro Vs Contra
  \+ Disinfectable mask body and adapter  
  \+ CPAP inline HEPA filter standard is same or above N95  
  \+ Suitable for long time use  
  \+ Relative sufficient supply  
             
  \- Inhale pressure test required (if too high, the assembly can be modified to become a positive pressure mask)  
  \- not tested yet  

## Design and Prototype
 
 ### Filter
   Here we aim at HEPA Grade Filter [A](http://store.flw.com/products/tsi-22-mm-iso-taper-hepa-grade-filter-1602292.html?gclid=Cj0KCQjwybD0BRDyARIsACyS8msJJZ74yWmaHp5G7_E8ZbKecEBjhoecMqpVVAeZVtnZAlM5y9SYJ9saAmeuEALw_wcB) [B](https://www.shoplet.com/Teleflex-Medical-Iso-Guard-HEPA-Light-Filter/9228022EA/spdv?pt=rk_frg_pla&gclid=Cj0KCQjwybD0BRDyARIsACyS8mu_AHg_uu-oBDI0UrMiQ9GWdhLPbSzsb2OK1AmrZptcMTd0lyIDhaAaAuOuEALw_wcB), which have a ISO Taper ⌀22mm outlet.
   ![filter](hepa-grade.png)


 ### Mask
   Here I used the mask made by [lafactoria3d](https://www.thingiverse.com/lafactoria3d/)

   ![thingiverse_mask](thingiverse_mask.jpg)

 ### Adapter
  Both parts are designed to print without support, test piece are printed on Formlab3
  - filter adapter is interchangable to fit ⌀22mm or ⌀18mm outlet standard
  - all fitting are taped for better sealing, test required.

  filter adapter  
   ![filter_adap](filter_adapter.jpg)  

  mask adapter  
   ![mask_adap](mask_adapter.jpg)

 ### Assembly
  A defect filter is used to test the fit
   ![assembly](assembly.jpg)
 
